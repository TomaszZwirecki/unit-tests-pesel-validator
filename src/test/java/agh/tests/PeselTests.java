package agh.tests;

import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.text.ParseException;

public class PeselTests {
    @Test
    public void incorrectPeselIndex() throws ParseException {
        Assert.assertEquals(PeselParser.Parse("48052944349").getDigit(-1), -1);
    }

}


