package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401358", false},
                {"44451401358", false},
                {"44651401358", false},
                {"44851401358", false},
                {"44321401358", false},
                {"4445140135", false},
                {"44851401358A", false},
                {"44851401358989", false},
                {"58073414795", false},
                {"93042612056", true},
                {"92022970231", false},
                {"94022834902", false},
        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void testPesel(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}




