package agh.tests;

import agh.qa.PeselExtractor;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.time.LocalDate;

public class PeselExtractorTests {
    @Test
    public void testGetBirthDate() throws ParseException {
        PeselExtractor extractor = new PeselExtractor(PeselParser.Parse("74120714637"));

        Assert.assertEquals(extractor.getBirthDate(), LocalDate.of(1974, 12, 7));

    }


    @DataProvider
    public Object[][] testGetsSex() {
        return new Object[][]{
                {"53022525514", "Male"},
                {"87072565264", "Female"}
        };
    }

    @Test(dataProvider = "testGetsSex")
    public void TestPesel(String pesel, String sex) throws ParseException {
        PeselExtractor extractor = new PeselExtractor(PeselParser.Parse(pesel));

        Assert.assertEquals(sex, extractor.getSex());
    }
}