package agh.tests;

import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.text.ParseException;

public class PeselParserTests {

    @Test
    public void testNotNumeric() {
        String notNumericPesel = "1111111111A";

        Assert.assertThrows(ParseException.class, () -> PeselParser.Parse(notNumericPesel));
    }

    @Test
    public void testLength() {
        String tooLongPesel = "111111111111";

        Assert.assertThrows(ParseException.class, () -> PeselParser.Parse(tooLongPesel));
    }
}
