package agh.qa;

import java.time.LocalDate;

public class PeselExtractor {
    private Pesel pesel;

    public PeselExtractor(Pesel pesel) {
        this.pesel = pesel;
    }

    public LocalDate getBirthDate() {
        return LocalDate.of(pesel.getBirthYear(), pesel.getBirthMonth(), pesel.getBirthDay());
    }

    public String getSex() {
        byte digit = pesel.getDigit(9);

        if (digit % 2 == 1) {
            return "Male";
        } else {
            return "Female";
        }
    }
}
